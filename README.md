# UARGOL
The Uart Clock Stretcher for Rigol scopes is a tiny PCB that stretches the
UART clock as the Rigol MS5000 uses an incorrect clock pulse.

While this may be related to a buggy implementation in the FPGA rerouting
logic, or on purpose to make debugging harder is unsure.

This little PCB stretches the clock to make the UART usable again.

Note, that this trick is only applied to the TX pin, the RX pin seems to
operate fine.
